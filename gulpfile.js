// Include gulp
const gulp = require('gulp');

// Include Our Plugins
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const del = require("del");
const sourcemaps = require('gulp-sourcemaps');
const browsersync = require('browser-sync').create();

const paths = {
    scripts: {
      src: './_assets/scripts/**/*.js',
      dest: './public/scripts/'
    },
    scss:{
        src:'./_assets/scss/**/*.scss',
        dest:'./public/css'
    },
    html:{
        src:'./_assets/*.html',
        dest:'./public/'
    },
    images:{
        src:'./_assets/images/**/*',
        dest:'./public/images'
    },
    vendors:{
        src:'./_assets/vendors/**/*',
        dest:'./public/vendors'
    }
  };

function css() {
  return gulp
    .src(paths.scss.src)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write(''))
    .pipe(gulp.dest(paths.scss.dest));
}

function scripts(){
    return gulp.src(paths.scripts.src)
    .pipe(gulp.dest(paths.scripts.dest));
}

function vendors(){
    return gulp.src(paths.vendors.src)
    .pipe(gulp.dest(paths.vendors.dest));
}

function images(){
    return gulp.src(paths.images.src)
    .pipe(gulp.dest(paths.images.dest));
}

function assets(){
    return gulp.src(paths.assets.src)
    .pipe(gulp.dest(paths.assets.dest));
}

function html(){
    return gulp.src(paths.html.src)
    .pipe(gulp.dest(paths.html.dest));
}


// Watch files
function watchFiles() {
    gulp.watch("./_assets/scss/**/*", css);
    gulp.watch("./_assets/scripts/**/*", scripts);
    gulp.watch("./_assets/images/**/*", images);
    gulp.watch("./_assets/vendors/**/*", vendors);
  }

  // define complex tasks
//const js = gulp.series(scriptsLint, scripts);
const public = gulp.parallel(css, images, html, scripts, vendors);
const watch = gulp.series(public, gulp.parallel(watchFiles));

// export tasks
exports.images = images;
exports.css = css;
exports.scripts = scripts;
//exports.clean = clean;
exports.public = public;
exports.watch = watch;
exports.default = public;