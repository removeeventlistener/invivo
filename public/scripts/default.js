var dpi = window.devicePixelRatio;

var chartCanvas;
var ctx;
var jsonData; //global holder of JSON data

function fix_dpi() {
    chartCanvas.width = document.body.clientWidth; 
    chartCanvas.height = document.body.clientHeight/ 2; 
    var myBarchart = new Barchart(
        {
            canvas:chartCanvas,
            data:jsonData,
        }
    );
    myBarchart.draw();
}

function drawLine(ctx, startX, startY, endX, endY,color){
    ctx.save();
    ctx.strokeStyle = color;
    ctx.beginPath();
    ctx.moveTo(startX,startY);
    ctx.lineTo(endX,endY);
    ctx.stroke();
    ctx.restore();
}

function drawBar(ctx, upperLeftCornerX, upperLeftCornerY, width, height,color){
    ctx.save();
    ctx.fillStyle=color;
    ctx.fillRect(upperLeftCornerX,upperLeftCornerY,width,height);
    ctx.restore();
}

function clearRect(ctx){
    ctx.save();
    ctx.FillStype= "white";
    ctx.clearRect(0, 0, chartCanvas.width, chartCanvas.height);
    ctx.restore();
}

var Barchart = function(options){
    this.padding = 10;

    this.options = options;
    this.canvas = options.canvas;
    this.ctx = this.canvas.getContext("2d");

    this.colors = options.colors;
  
    this.draw = function(){


        //Grab our dropdown variable
        var targetData = document.getElementById("dataSet").value;

        var maxValue = 0;
        for (var categ in this.options.data){
            maxValue = Math.max(maxValue,this.options.data[categ][targetData]);
        }
        var canvasActualHeight = this.canvas.height - this.padding * 2;
        var canvasActualWidth = this.canvas.width - this.padding * 2;
        
        var gridScaleY = Math.floor(maxValue/10);
        clearRect(this.ctx);
        
        //drawing the grid lines
        var gridValue = 0;
        while (gridValue <= maxValue){
            var gridY = canvasActualHeight * (1 - gridValue/maxValue) + this.padding;
            drawLine(this.ctx, 0, gridY, this.canvas.width, gridY, "black");
             
            //writing grid markers
            this.ctx.save();
            this.ctx.fillStyle = "black";
            this.ctx.textBaseline="bottom"; 
            this.ctx.font = "bold 10px Arial";
            this.ctx.fillText(gridValue, 10, gridY - 2);
            this.ctx.restore();
 
            gridValue += gridScaleY;
        }
  
        //drawing the bars
        var barIndex = 0;
        var numberOfBars = Object.keys(this.options.data).length;
        var barSize = (canvasActualWidth)/numberOfBars;
 
        for (categ in this.options.data){
            var val = this.options.data[categ][targetData];
            var barHeight = Math.round( canvasActualHeight * val/maxValue) ;
            drawBar(
                this.ctx,
                50 + barIndex * barSize + (2 * barIndex),
                this.canvas.height - barHeight - this.padding,
                barSize,
                barHeight,
                randomColour()
            );
 
            barIndex++;
        }

        //drawing series name
        this.ctx.save();
        this.ctx.textBaseline="bottom";
        this.ctx.textAlign="center";
        this.ctx.fillStyle = "#000000";
        this.ctx.font = "bold 14px Arial";
        this.ctx.fillText(targetData, this.canvas.width/2,this.canvas.height);
        this.ctx.restore();  

    }
}

function randomColour(){
    return  "#" + Math.floor(Math.random()*16777215).toString(16);
}

function initProvinceDropDown(){
    ajax.get('api/Provinces', null, function(data) {
        var provinceData = JSON.parse(data);
        //Append our data into the dropdown list.
        var template = document.getElementById('provinceObject').innerHTML;
        provinceData.forEach(function(value){
            var rendered = Mustache.render(template, value);
            //document.getElementById('provinces').appendChild(rendered);
            document.getElementById('provinces').insertAdjacentHTML('beforeend', rendered);
        });

        //init our barchart
        initCanvas();

        //Init our UI
        initUI();
    });
}

function initCanvas(){
    chartCanvas = document.getElementById("chart");
    chartCanvas.width = document.body.clientWidth; //document.width is obsolete
    chartCanvas.height = document.body.clientHeight/ 2; //document.height is obsolete
    ctx = chartCanvas.getContext("2d");
    QueryAPI();
}


function QueryAPI(){

    var query = {};
    query.province = document.getElementById("provinces").value;
    query.dateFrom = document.getElementById("dateFrom").value;
    query.dateTo = document.getElementById("dateTo").value;
    query.dataSet = document.getElementById("dataSet").value;

    ajax.post('api/covid/query', query, function(data) {
        jsonData = JSON.parse(data);

        var myBarchart = new Barchart(
            {
                canvas:chartCanvas,
                data:jsonData,
            }
        );
        myBarchart.draw();
    });
}

function initUI(){
    addEventHandler(document.getElementById('dateFrom'), 'change', function() {
        QueryAPI();
    });
    addEventHandler(document.getElementById('dateTo'), 'change', function() {
        QueryAPI();
    });
    addEventHandler(document.getElementById('provinces'), 'change', function() {
        QueryAPI();
    });
    addEventHandler(document.getElementById('dataSet'), 'change', function() {
        QueryAPI();
    });  
}

function addEventHandler(elem, eventType, handler) {
    if (elem.addEventListener)
        elem.addEventListener (eventType, handler, false);
    else if (elem.attachEvent)
        elem.attachEvent ('on' + eventType, handler); 
}


function initApp(){
    console.log('init');

    //Grab our provinces
    initProvinceDropDown();


}




//Wait for document to be ready then init the app
document.addEventListener("DOMContentLoaded", function(event) {
    initApp();

    window.onresize = fix_dpi;
});
