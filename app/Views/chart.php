<div class="container-fluid">
    <div class="row">
        <div class="col">
            <h1>Epidemiological Data from the COVID-19 Outbreak in Canada</h1>
            <p>Data Source: https://github.com/ccodwg/Covid19Canada</p>
            <p>Data Valid up to July 9, 2021</p>
        </div>
    </div>
    
    <div class="row">
        <div class="col" >
            <div class="card">
                <div class="card-body">
                    <canvas id="chart"></canvas>
                </div>
            </div>
        </div>
    </div>
    <form id="dataForm">
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <label for="DateFrom" class="form-label">Date From</label>
                <input type="date" class="form-control" id="dateFrom" value="2021-05-09">
                <label for="DateTo" class="form-label">Date To</label>
                <input type="date" class="form-control" id="dateTo" value="2021-07-09">
            </div>
            <div class="col-xs-12 col-md-4">
                <label for="provinces" class="form-label">Provinces</label>
                <select class="form-select" aria-label="Provinces" id="provinces">
                </select>
            </div>
            <div class="col-xs-12 col-md-4">
                <label for="inputEmail4" class="form-label">Data Sets</label>
                <select class="form-select" aria-label="Provinces" id="dataSet">
                    <option value="active_cases">active_cases</option>
                    <option value="cumulative_cases">cumulative_cases</option>
                    <option value="cumulative_recovered">cumulative_recovered</option>
                    <option value="cumulative_deaths">cumulative_deaths</option>
                    <option value="active_cases_change">active_cases_change</option>
                </select>
            </div>
        </div>
    </form>
</div>

<script type="text/html" id="provinceObject">
    <option value="{{targetValue}}">{{name}}</option>
</script>