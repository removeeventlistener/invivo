<?php

namespace App\Models;

use CodeIgniter\Model;

class CovidModel extends Model
{

    protected $table      = 'active_timeline_province';
    protected $primaryKey = 'id';
    protected $allowedFields = ['province', 'date_active', 'cumulative_cases', 'cumulative_recovered', 'cumulative_deaths', 'active_cases', 'active_cases_change'];


    public function all(){
        return $this->findAll();
    }

    public function query($province, $dateFrom, $dateTo, $fields=[]){
        $builder = $this->db->table($this->table);
        foreach($fields as $field){
            if ($this->db->fieldExists($field, $this->table)){
                $builder->select($field);
            }
        }

        $builder->where('province', $province);
        $builder->where('date_active >=', $dateFrom);
        $builder->where('date_active <=', $dateTo);

        return $builder->get()->getResultArray();
    }
}