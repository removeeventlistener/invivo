<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		echo view('global/header');
		echo view('chart');
		echo view('global/footer');
	}
}
