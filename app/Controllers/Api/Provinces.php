<?php

namespace App\Controllers\Api;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\ProvinceModel;

class Provinces extends ResourceController
{
    use ResponseTrait;
    
    public function index()
    {
        $model = new ProvinceModel();
        $data = $model->findAll();
        if ($data != null){
            return $this->respond($data, 200);
        }else{
            return $this->failNotFound('no data!');
        }
    }

    public function show($id = null){
        $model = new ProvinceModel();
        $data = $model->find($id);

        if ($data){
            return $this->respond($data, 200);
        }else{
            return $this->failNotFound('Data not found');
        }
    }


}