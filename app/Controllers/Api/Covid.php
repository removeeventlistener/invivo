<?php

namespace App\Controllers\Api;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\CovidModel;

class Covid extends ResourceController
{
    use ResponseTrait;
    
    public function index()
    {
        $model = new CovidModel();
        $data = $model->all();
        if ($data != null){
            return $this->respond($data, 200);
        }else{
            return $this->failNotFound('no data!');
        }
    }

    public function show($id = null){
        $model = new CovidModel();
        $data = $model->find($id);

        if ($data){
            return $this->respond($data, 200);
        }else{
            return $this->failNotFound('Data not found');
        }
    }

    public function query(){
        $province = $this->request->getPost("province") ? $this->request->getPost("province") : "Ontario";
        $dataSet = $this->request->getPost("dataSet") ? $this->request->getPost("dataSet") : "cumulative_cases";
        $dateFrom = $this->request->getPost("dateFrom") ? $this->request->getPost("dateFrom") : date('Y-m-d', strtotime(date("Y-m-d") . " -31 days"));
        $dateFrom = !empty($dateFrom) ? $dateFrom :  date('Y-m-d', strtotime(date("Y-m-d") . " -31 days"));
        $dateTo = $this->request->getPost("dateTo") ? $this->request->getPost("dateTo") : date("Y-m-d");
        $dateTo = !empty($dateTo) ? $dateTo : date("Y-m-d");


        $queryArray = ["id", "province", "date_active"];
        $queryArray[] = $dataSet;

        $model = new CovidModel();
        $data = $model->query($province, $dateFrom, $dateTo, $queryArray);

        if ($data){
            return $this->respond($data, 200);
        }else{
            return $this->failNotFound('error at Query');
        }  
    }

}