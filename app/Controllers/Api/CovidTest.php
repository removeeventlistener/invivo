<?php

namespace App\Controllers\Api;
use App\Models\CovidModel;
use App\Controllers\BaseController;

class CovidTest extends BaseController
{

    public function show($id = null){
        $model = new CovidModel();
        $data = $model->find($id);

        if ($data){
            return $this->respond($data, 200);
        }else{
            return $this->failNotFound('Data not found');
        }
    }

    public function query(){
        $province = $this->request->getPost("province") ? $this->request->getPost("province") : "Ontario";
        $cumulative_cases = strtoupper($this->request->getPost("cumulative_cases")) == "ON" ? true : false;
        $cumulative_recovered = strtoupper($this->request->getPost("cumulative_recovered")) == "ON" ? true : false;
        $cumulative_deaths = strtoupper($this->request->getPost("cumulative_deaths")) == "ON" ? true : false;
        $active_cases = strtoupper($this->request->getPost("active_cases")) == "ON" ? true : false;
        $active_cases_change = strtoupper($this->request->getPost("active_cases_change")) == "ON" ? true : false;

        $dateFrom = $this->request->getPost("dateFrom") ? $this->request->getPost("dateFrom") : date('Y-m-d', strtotime(date("Y-m-d") . " -7 days"));
        $dateTo = $this->request->getPost("dateTo") ? $this->request->getPost("dateTo") : date("Y-m-d");

        var_dump($dateFrom, $dateTo);
        $queryArray = ["id", "province", "date_active", "active_cases"];
        if ($cumulative_cases){
            $queryArray[] = "cumulative_cases";
        }
        if ($cumulative_recovered){
            $queryArray[] = "cumulative_recovered";
        }
        if ($cumulative_deaths){
            $queryArray[] = "cumulative_deaths";
        }
        if ($active_cases){
            $queryArray[] = "active_cases";
        }
        if ($active_cases_change){
            $queryArray[] = "active_cases_change";
        }


        $model = new CovidModel();
        $data = $model->query($province, $dateFrom, $dateTo, $queryArray);

        var_dump($data);
    }

}