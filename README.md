# Inivo Test

# Requirements
* PHP ver 7.2+  Best with Ver 8.0
* php_intl - THIS IS A MUST.  XAMP on Windows has a DLL available to install.  On Mac either manually compile or use MAMP
* MySQL or mariaDB
* PHPMyAdmin installed locally
* Composer
* Node ver 14+  Do no run Node ver 15+ as the package.json has not bee updated for that version.

#Supported Browser
* Chrome
* Firefox
* Safari
* Edge

# Installation
* Extract files to a directory of your choice
* Open up your PHPMyAdmin in your browser
* in PHPMyAdmin import database.sql into your database.  You should see a table called "invivo" created. 
* Copy the env file and rename it to .env to create your own custom enviroment file.
* Open the .env file in your favourite editor
* At Line 17 uncomment it and change it so it looks ike this

`CI_ENVIRONMENT = Development`

* At line 40, uncomment all lines all the way to line 45.
* at line 40, fill in your DB hostname
* at line 41, change the DB name to invivo

`database.default.database = invivo`

* at line 41 and 42 change the login to your DB login/password combination.
* Save the .env file.

# Runnning Application
* Open a Terminal WIndow of your choice.
* Navigate to your downloaded code repistory
* Type `php spark serve` to run your local server.
* Open a browser and navigate to the url http://localhost:8080/ to see the site

